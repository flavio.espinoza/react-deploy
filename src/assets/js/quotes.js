export default function (author) {
  function handleError (errorMessage) {
      return {
        success: false,
        author: null,
        preferredName: null,
        quote: errorMessage,
        title: null,
        website: null,
        email: null,
      }
  }
  function fullName(author) {
    return `${author.firstName} ${author.middleName ? author.middleName : ''} ${author.lastName}`
  }
  if (!author || !author.firstName || !author.lastName) {
    let errorMessage = 'The first argument must be an Object: { firstName: String, middleName: String (optional), lastName: String }'
    return handleError(errorMessage)
  }
  if (author.firstName.toLowerCase() === `kevin`) {
    if (author.lastName.toLowerCase() === `burke`) {
      return {
        success: true,
        author: 'Kevin Burke, AIA',
        preferredName: 'Kevin',
        quote: `"Flavio is a rare combination of talent and commitment with an intuitive understanding of 
                graphic visual communication and a propensity for creative problem-solving. Flavio considers the broad 
                ramifications of a concept even while he is designing the smallest detail."`,
        title: 'Principle',
        company: 'Parabola Architecture',
        website: 'https://parabola-architecture.com/',
        email: 'burke614@gmail.com',
      }
    }
    let errorMessage = `An author with the name "${fullName(author)}" was not found.`
    return handleError(errorMessage)
  }
}
